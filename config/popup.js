document.getElementById('config-form').addEventListener('submit', function(event) {
  event.preventDefault();
  chrome.storage.local.set({
    'url': document.getElementById('url-field').value,
    'scale': document.getElementById('scale-field').value,
  });
  document.getElementById('form-button').disabled = true;
});

['url-field', 'scale-field'].forEach(function(el) {
  document.getElementById(el).addEventListener('input', function() {
    document.getElementById('form-button').disabled = false;
  });
})

chrome.storage.local.get({'url': 'https://example.com/', 'scale': 1}, function(value) {
  document.getElementById('url-field').value = value.url;
  document.getElementById('scale-field').value = value.scale;
});
